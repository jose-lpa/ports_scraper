from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from ports_scraper.settings import (
    DATABASE_HOST,
    DATABASE_NAME,
    DATABASE_PASSWORD,
    DATABASE_USER
)


engine = create_engine(
    'postgresql+psycopg2://{user}:{password}@{host}/{name}'.format(
        user=DATABASE_USER,
        password=DATABASE_PASSWORD,
        host=DATABASE_HOST,
        name=DATABASE_NAME
    ),
    client_encoding='utf8'
)

Session = sessionmaker(bind=engine)
